'use strict'

const resolve = require('path').resolve

module.exports = {

  modules: [
    '@nuxtjs/axios',

  ],
  axios: {
    baseURL: 'https://app.influway.com'
  },

  build: {
    extractCSS: true
  },

  plugins: [

    '~plugins/axios',
    { src: '~plugins/axios', ssr: false },

    '~plugins/iview',
    { src: '~plugins/iview', ssr: false }

  ],


  /*
  ** Headers of the page
  */
  head: {
    title: 'InfluWay | Decentralized social e-commerce platform',
    meta: [
      {
        charset: 'utf-8'
      },
      {
        name: 'viewport',
        content: 'width=device-width, initial-scale=1'
      },
      
    ],
    link: [

      // vendor 
      // {
      //   rel: 'stylesheet',
      //   href: '/vendor/css/bootstrap.min.css'
      // },
      // <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700' rel='stylesheet' type='text/css'>
      {
        rel: 'icon',
        type: 'image/x-icon',
        href: 'favicon.png'
      },
      
      {
        rel: 'stylesheet',
        href: 'https://fonts.googleapis.com/css?family=Open+Sans:400,600,700',
        type: 'text/css'
      },

      {
        rel: 'stylesheet',
        href: 'https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.0/animate.min.css'
      },
      {
        rel: 'stylesheet',
        href: 'https://use.fontawesome.com/releases/v5.1.0/css/all.css'
      },
      {
        rel: 'stylesheet',
        href: 'https://fonts.googleapis.com/css?family=Slabo+27px'
      },
      // {
      //   rel: 'stylesheet',
      //   href: '/vendor/css/bootstrap.min.css'
      // },
      // {
      //   rel: 'stylesheet',
      //   href: '/vendor/css/owl.carousel.min.css'
      // },
      // {
      //   rel: 'stylesheet',
      //   href: '/vendor/css/owl.theme.default.min.css'
      // },
      // {
      //   rel: 'stylesheet',
      //   href: '/vendor/css/style.css'
      // },


      // retail 
      {
        rel: 'stylesheet',
        href: '/retail/css/bootstrap.min.css'
      },
      {
        rel: 'stylesheet',
        href: '/retail/style.css'
      },
      {
        rel: 'stylesheet',
        href: 'https://fonts.googleapis.com/css?family=Lato:400,700%7CMontserrat:300,400,600,700'
      },
      {
        rel: 'stylesheet',
        href: '/retail/icons/fontawesome/css/fontawesome-all.min.css'
      },{
        rel: 'stylesheet',
        href: '/retail/icons/Iconsmind__Ultimate_Pack/Line%20icons/styles.min.css'
      },

      /* <link rel="stylesheet" href="css/bootstrap.min.css">
		<link rel="stylesheet" href="style.css">
		<link rel="stylesheet" href="//fonts.googleapis.com/css?family=Lato:400,700%7CMontserrat:300,400,600,700">
		
		<link rel="stylesheet" href="icons/fontawesome/css/fontawesome-all.min.css"><!-- FontAwesome Icons -->
		<link rel="stylesheet" href="icons/Iconsmind__Ultimate_Pack/Line%20icons/styles.min.css"><!-- iconsmind.com Icons --></link>
       */
    ],

    script:[

      // vendor
      // { src: '/vendor/js/jquery-3.3.1.slim.min.js', body: true },
      // { src: '/vendor/js/popper.min.js', body: true },
      // { src: '/vendor/js/bootstrap.min.js', body: true },
      // { src: '/vendor/js/owl.carousel.min.js', body: true },
      // { src: '/vendor/js/custom.js', body: true },
      // { src: '/vendor/js/custom-vendor.js', body: true },
      //{ src: 'https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js', body: true },
      


      // retail

      { src: '/retail/js/libs/jquery-3.3.1.min.js', body: true },
      { src: '/retail/js/libs/popper.min.js', body: true },
      { src: '/retail/js/libs/bootstrap.min.js', body: true },
      { src: '/retail/js/navigation.js', body: true },
      { src: '/retail/js/jquery.flexslider-min.js', body: true },
      { src: '/retail/js/jquery-asRange.min.js', body: true },
      { src: '/retail/js/circle-progress.min.js', body: true },
      { src: '/retail/js/afterglow.min.js', body: true },



      { src: '/vendor/js/custom-vendor.js', body: true },


      { src: '/retail/js/script.js', body: true },
      { src: '/retail/js/script-dashboard.js', body: true }


    //   <!-- Required Framework JavaScript -->
		// <script src="js/libs/jquery-3.3.1.min.js"></script><!-- jQuery -->
		// <script src="js/libs/popper.min.js" defer></script><!-- Bootstrap Popper/Extras JS -->
		// <script src="js/libs/bootstrap.min.js" defer></script><!-- Bootstrap Main JS -->
		// <!-- All JavaScript in Footer -->
		
		// <!-- Additional Plugins and JavaScript -->
		// <script src="js/navigation.js" defer></script><!-- Header Navigation JS Plugin -->
		// <script src="js/jquery.flexslider-min.js" defer></script><!-- FlexSlider JS Plugin -->
		// <script src="js/jquery-asRange.min.js" defer></script><!-- Range Slider JS Plugin -->
		// <script src="js/circle-progress.min.js" defer></script><!-- Circle Progress JS Plugin -->
		// <script src="js/afterglow.min.js" defer></script><!-- Video Player JS Plugin -->
		// <script src="js/script.js" defer></script><!-- Custom Document Ready JS -->
		// <script src="js/script-dashboard.js" defer></script><!-- Custom Document Ready for Dashboard Only JS -->
		
  ],

//   <script src="js/jquery-3.3.1.slim.min.js"></script>
//   <script src="js/popper.min.js"></script>
//   <script src="js/bootstrap.min.js" ></script>
// <script src="js/owl.carousel.min.js"></script>
  },

  // <title>Sylhet-town-hotel</title>
  //   <!-- Bootstrap CSS -->
  //   <link rel="stylesheet" href="css/bootstrap.min.css">
	// <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.0/animate.min.css">
	// <!--Font-awsome-->
	// <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css">
	// <!--Google font-->
	// <link href="https://fonts.googleapis.com/css?family=Slabo+27px" rel="stylesheet">
	// <!--owl-carousel-->
	// <link rel="stylesheet" href="css/owl.carousel.min.css">
	// <link rel="stylesheet" href="css/owl.theme.default.min.css">
	// <!-- Material Design Bootstrap -->
 
	// <!--CSS-->
  // <link rel="stylesheet" href="css/style.css"></link>
  
  /*
  ** Global CSS
  */
  // css: ['css/main.css'],
  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#744d82' },
  /*
  ** Point to resources
  */
  srcDir: resolve(__dirname, '..', 'resources')
}
