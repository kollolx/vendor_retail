import Vue from 'vue'

Vue.mixin({

    methods: {

        async callAxios(method, url, dataObj) {

            try {

                let data = await this.$axios({
                    method: method,
                    url: url,
                    data: dataObj
                })
                return data

            } catch (e) {

                return e.response
            }
        },
    }
})